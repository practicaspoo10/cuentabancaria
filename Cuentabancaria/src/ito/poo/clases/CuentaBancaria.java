package ito.poo.clases;

import java.time.LocalDate;

public class CuentaBancaria {
	
		private long numeroCuenta = 0L;
		private String nombreCliente = "";
		private float saldo = 0F;
		private LocalDate fechaAper = null;
		private LocalDate fechaActual = null;
		/*******/
		public CuentaBancaria() {
			super();
		}
		public CuentaBancaria(long numeroCuenta, String nombreCliente, float saldo, LocalDate fechaAper) {
			super();
			this.numeroCuenta = numeroCuenta;
			this.nombreCliente = nombreCliente;
			this.saldo = saldo;
			this.fechaAper = fechaAper;
		}
		/*******/
		public boolean Deposito(float Cantidad,LocalDate newfechaActual) {
			boolean Deposito = false;
			if(this.fechaAper==null)
				System.out.println("La cuenta no se encuentra activa");
			else {
				Deposito = true;
				this.setSaldo(this.getSaldo()+ Cantidad);
				this.setFechaActual(newfechaActual);
			}
			return Deposito;
		}

		public boolean Retiro (float Cantidad,LocalDate newFechaActual) {
			boolean Retiro = false;
			if (Cantidad <= this.getSaldo()) {
				 Retiro=true;
				 this.setSaldo(this.getSaldo()-Cantidad);
				 this.setFechaActual(newFechaActual);
				}
			else
				System.out.println("La cantidad sobrepasa el saldo a retirar:(");
			return Retiro;
		}
		/*******/
		public long getNumeroCuenta() {
			return numeroCuenta;
		}

		public void setNumeroCuenta(long numeroCuenta) {
			this.numeroCuenta = numeroCuenta;
		}

		public String getNombreCliente() {
			return nombreCliente;
		}

		public float getSaldo() {
			return saldo;
		}

		public void setSaldo(float saldo) {
			this.saldo = saldo;
		}

		public LocalDate getFechaAper() {
			return fechaAper;
		}

		public void setFechaAper(LocalDate fechaAper) {
			this.fechaAper = fechaAper;
		}

		public LocalDate getFechaActual() {
			return fechaActual;
		}

		public void setFechaActual(LocalDate fechaActual) {
			this.fechaActual = fechaActual;
		}
		/*******/
		@Override
		public String toString() {
			return "CuentaBancaria [numeroCuenta=" + numeroCuenta + ", nombreCliente=" + nombreCliente + ", saldo=" + saldo
					+ ", fechaAper=" + fechaAper + ", fechaActual=" + fechaActual + "]";
		}
		/*******/
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((fechaActual == null) ? 0 : fechaActual.hashCode());
			result = prime * result + ((fechaAper == null) ? 0 : fechaAper.hashCode());
			result = prime * result + ((nombreCliente == null) ? 0 : nombreCliente.hashCode());
			result = prime * result + (int) (numeroCuenta ^ (numeroCuenta >>> 32));
			result = prime * result + Float.floatToIntBits(saldo);
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			CuentaBancaria other = (CuentaBancaria) obj;
			if (fechaActual == null) {
				if (other.fechaActual != null)
					return false;
			} else if (!fechaActual.equals(other.fechaActual))
				return false;
			if (fechaAper == null) {
				if (other.fechaAper != null)
					return false;
			} else if (!fechaAper.equals(other.fechaAper))
				return false;
			if (nombreCliente == null) {
				if (other.nombreCliente != null)
					return false;
			} else if (!nombreCliente.equals(other.nombreCliente))
				return false;
			if (numeroCuenta != other.numeroCuenta)
				return false;
			if (Float.floatToIntBits(saldo) != Float.floatToIntBits(other.saldo))
				return false;
			return true;
		}
		public int compareTo(CuentaBancaria arg0) {
			int r=0;
			if(this.numeroCuenta != arg0.getNumeroCuenta())
				return this.numeroCuenta>arg0.getNumeroCuenta()?1:-1;
			else if (this.saldo != arg0.getSaldo())
				return this.saldo>arg0.getSaldo()?2:-2;
			else if (!this.nombreCliente.equals(arg0.getNombreCliente()))
					return this.nombreCliente.compareTo(arg0.getNombreCliente());
			else if (!this.fechaAper.equals(arg0.getFechaAper()))
				return this.fechaAper.compareTo(arg0.getFechaAper());
			return r;
		}

	}


