package ito.poo.app;

import java.time.LocalDate;

import ito.poo.clases.CuentaBancaria;

public class MyApp {
	
	public static void run() {
		CuentaBancaria cb = new CuentaBancaria(25486470L,"Elena Toledo",77000F,LocalDate.of(2020, 8, 15));
		System.out.println(cb);
		System.out.println(cb.Retiro(32000F, LocalDate.of(2021, 2, 24)));
		System.out.println(cb);
		System.out.println(cb.Deposito(23000F, LocalDate.of(2021, 7, 11)));
		System.out.println(cb);
		
		System.out.println();
		System.out.println();
		
		CuentaBancaria cb1 = new CuentaBancaria(25486470L,"Elena Toledo",77000F,null);
		System.out.println(cb1);
		System.out.println(cb1.Retiro(75000F, LocalDate.of(2021, 6, 20)));
		System.out.println(cb1);
		System.out.println(cb1.Deposito(23000F, LocalDate.of(2021, 6, 21)));
		System.out.println(cb1);
		
		System.out.println();
		System.out.println();

		CuentaBancaria cb2 = new CuentaBancaria(17855645L,"Elena Toledo",70000F,LocalDate.of(2021, 6, 21));
		System.out.println(cb2);
		System.out.println(cb2.Retiro(25000F, LocalDate.of(2021, 6, 20)));
		System.out.println(cb2);
		System.out.println(cb2.Deposito(30000F, LocalDate.of(2021, 6, 25)));
		System.out.println(cb2);
		
		System.out.println();
		System.out.println("/*********************************/");
		System.out.println();
		
		CuentaBancaria cb3 = new CuentaBancaria(17855645L,"Elena Toledo",50000F,LocalDate.of(2021, 5, 20));
		System.out.println(cb3);
		System.out.println(cb3.Retiro(25000F, LocalDate.of(2021, 8, 12)));
		System.out.println(cb3);
		System.out.println(cb3.Deposito(30000F, LocalDate.of(2021, 9, 30)));
		System.out.println(cb3);
			
		System.out.println();
		System.out.println();

		
		
		System.out.println(!cb.equals(cb3));
	    System.out.println(cb3.compareTo(cb));
	    
	    System.out.println(!cb2.equals(cb2));
	    System.out.println(cb.compareTo(cb2));
	}
	public static void main(String [] args) {
		run();
	}

}
